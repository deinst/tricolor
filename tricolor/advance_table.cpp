//
//  advance_table.cpp
//  tricolor
//
//  Created by David Einstein on 1/26/14.
//  Copyright (c) 2014 David Einstein. All rights reserved.
//

#include "advance_table.h"
#include <iterator>
#include <algorithm>
#include <set>
using std::set;
using std::max;

dynamic_row next_row(const dynamic_row &last_row,
                     int num_rows,
                     const vector<index_list> &matches,
                     const vector<weight_list> &weights,
                     const int max_weight) {
    dynamic_row ret;
    std::cout << "last row size: " << last_row.size() << std::endl;
    for (auto rit : last_row) {
        const dynamic_cell &crow = rit.first;
        for (index_t index : matches[crow.index]) {
            dynamic_cell newv;
            newv.index = index;
            const weight_list &weight = weights[index];
            newv.weights[0] = crow.weights[0] + weight[0];
            newv.weights[1] = crow.weights[1] + weight[1];
            newv.weights[2] = crow.weights[2] + weight[2];
            if (max(newv.weights[0], max(newv.weights[1], newv.weights[2])) <= max_weight) {
                ret[newv] += rit.second;
            }
        }
     }
    std::cout << "next row size: " << ret.size() << std::endl;
    return ret;
}

index_list get_table_at(mpz_class &pos, const vector<dynamic_row> &drows,
                         const vector<index_list> &matches,
                         const vector<weight_list> &weights) {
    index_list ret;
    // first get the top element
    const dynamic_row& top_row = drows.back();
    mpz_class run = 0;
    mpz_class prev = 0;
    mpz_class remain = pos;
    const dynamic_cell *vecp = 0;
    for (auto rit = top_row.begin(); rit != top_row.end(); ++rit) {
        run += rit->second;
        if (run > remain) {
            remain -= prev;
            vecp = &rit->first;
            ret.push_back(vecp->index);
            break;
        }
        prev = run;
    }
    if (vecp == 0) {
        std::cout << "crap" << std::endl;
        return ret;
    }
    for (auto drow = drows.rbegin() + 1; drow != drows.rend(); ++drow) {
        run = 0;
        prev = 0;
        int curr_row = vecp->index;
        dynamic_cell newrow;
        newrow.weights[0] = vecp->weights[0] - weights[vecp->index][0];
        newrow.weights[1] = vecp->weights[1] - weights[vecp->index][1];
        newrow.weights[2] = vecp->weights[2] - weights[vecp->index][2];
        vecp = 0;
        for (auto adjit = matches[curr_row].begin(); adjit != matches[curr_row].end(); ++adjit) {
            newrow.index = *adjit;
            auto newit = drow->find(newrow);
            if (newit != drow->end()) {
                run += newit->second;
                if (run > remain) {
                    remain -= prev;
                    vecp = &newit->first;
                    ret.push_back(*adjit);
                    break;
                }
                prev = run;
            }
        }
        if (vecp == 0) {
            std::cout << "crap" << std::endl;
            return ret;
        }        
    }
    return ret;
}

dynamic_row next_row_z(const dynamic_row &last_row,
                       int num_rows,
                       const vector<index_list> &matches,
                       const vector<weight_list> &weights,
                       const int max_weight,
                       const vector<root_t> &roots) {
    dynamic_row ret;
    dynamic_cell transv;
    dynamic_cell newv;
    std::cout << "last row size: " << last_row.size() << std::endl;
    for (auto rit : last_row) {
        const dynamic_cell &crow = rit.first;
        for (index_t index : matches[crow.index]) {
            newv.index = index;
            const weight_list &weight = weights[index];
            newv.weights[0] = crow.weights[0] + weight[0];
            newv.weights[1] = crow.weights[1] + weight[1];
            newv.weights[2] = crow.weights[2] + weight[2];
            if (max(newv.weights[0], max(newv.weights[1], newv.weights[2])) <= max_weight) {
                const root_t &root = roots[index];
                transv.index = root.index;
                transv.weights[0] = newv.weights[root.permutation[0]];
                transv.weights[1] = newv.weights[root.permutation[1]];
                transv.weights[2] = newv.weights[root.permutation[2]];
                ret[transv] += rit.second;
            }
        }
    }
    return ret;
}

index_list get_table_at_z(mpz_class &pos, const vector<dynamic_row> &drows,
                           const vector<index_list> &matches,
                           const vector<weight_list> &weights,
                          const vector<root_t> &roots,
                          const vector<newcolor_list> &newcolor_lists) {
    index_list ret;
    // first get the top element
    mpz_class bigcoloring = pos % 6;
    coloring_t coloring = (coloring_t)bigcoloring.get_ui();
    
    const dynamic_row& top_row = drows.back();
    mpz_class run = 0;
    mpz_class prev = 0;
    mpz_class remain = pos;
    const dynamic_cell *vecp = 0;
    for (auto rit : top_row) {
        run += rit.second;
        if (run > remain) {
            remain -= prev;
            vecp = &rit.first;
            newcolor_t next = newcolor_lists[vecp->index][coloring];
            coloring = next.second;
            ret.push_back(next.first);
            break;
        }
        prev = run;
    }
    if (vecp == 0) {
        std::cout << "crap" << std::endl;
        return ret;
    }
    for (auto drow = drows.rbegin() + 1; drow != drows.rend(); ++drow) {
        run = 0;
        prev = 0;
        int curr_row = vecp->index;
        dynamic_cell newrow;
        dynamic_cell rootrow;
        newrow.weights[0] = vecp->weights[0] - weights[vecp->index][0];
        newrow.weights[1] = vecp->weights[1] - weights[vecp->index][1];
        newrow.weights[2] = vecp->weights[2] - weights[vecp->index][2];
        vecp = 0;
        for (index_t index : matches[curr_row]) {
            const root_t &root = roots[index];
            rootrow.index = root.index;
            rootrow.weights[0] = newrow.weights[root.permutation[0]];
            rootrow.weights[1] = newrow.weights[root.permutation[1]];
            rootrow.weights[2] = newrow.weights[root.permutation[2]];
            auto newit = drow->find(rootrow);
            if (newit != drow->end()) {
                run += newit->second;
                if (run > remain) {
                    remain -= prev;
                    vecp = &newit->first;
                    newcolor_t next = newcolor_lists[index][coloring];
                    coloring = next.second;
                    ret.push_back(next.first);
//                    ret.push_back(index);
                    break;
                }
                prev = run;
            }
        }
        if (vecp == 0) {
            std::cout << "crap" << std::endl;
            return ret;
        }
    }
    return ret;
}

