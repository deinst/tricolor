//
//  check_match.h
//  tricolor
//
//  Created by David Einstein on 1/25/14.
//  Copyright (c) 2014 David Einstein. All rights reserved.
//

#ifndef __tricolor__check_match__
#define __tricolor__check_match__

#include <vector>
#include <iostream>
#include <map>
#include <gmpxx.h>
#include "row_gen.h"

using std::vector;

bool check_no_match(const row_t &a, const row_t &b);
int count_matches(const row_t &a, const vector<row_t> &rows);
vector<row_t> get_matches(const row_t &a, const vector<row_t> &rows);
typedef std::map<row_t, vector<row_t> > matches_t;
bool validate(const index_list &table, const vector<row_t> &rows);
#endif /* defined(__tricolor__check_match__) */
