//
//  check_match.cpp
//  tricolor
//
//  Created by David Einstein on 1/25/14.
//  Copyright (c) 2014 David Einstein. All rights reserved.
//

#include "check_match.h"

bool check_no_match(const row_t &a, const row_t &b){
    for (auto ait = a.begin(), bit = b.begin(); ait != a.end() && bit != b.end(); ++ait, ++bit) {
        if ((*ait) == (*bit)) {
            return false;
        }
    }
    return true;
}

int count_matches(const row_t &a, const vector< row_t > &rows) {
    int count = 0;
    for (row_t rit : rows) {
        if (check_no_match(a, rit)) {
            ++count;
        }
    }
    return count;
}

vector<row_t> get_matches(const row_t &a, const vector<row_t> &rows) {
    vector<row_t> ret;
    for (row_t rit : rows) {
        if (check_no_match(a, rit)) {
            ret.push_back(rit);
        }
    }
    return ret;
}

bool validate(const index_list &table, const vector<row_t> &rows) {
    vector<int> counts(3, 0);
    for (index_t index : table) {
        const row_t &row = rows[index];
        for (color col : row) {
            counts[col]++;
        }
    }
    if (counts[0] != counts[1] || counts[0]!=counts[2]) {
        return false;
    }
    for (auto indit = table.begin(); indit + 1 != table.end(); ++indit) {
        auto nexit = indit + 1;
        const row_t &row1 = rows[*indit];
        const row_t &row2 = rows[*nexit];
        for (auto col1 = row1.begin(), col2 = row2.begin();
             col1 != row1.end() && col2 != row2.end(); ++col1, ++ col2) {
            if (col1 == col2) {
                return false;
            }
        }
    }
    return true;
}