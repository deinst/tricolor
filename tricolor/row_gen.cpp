//
//  row_gen.cpp
//  tricolor
//
//  Created by David Einstein on 1/25/14.
//  Copyright (c) 2014 David Einstein. All rights reserved.
//
#include <cassert>
#include "row_gen.h"

// generate a table of all the possible row colorings
vector<row_t > genrows(int numcols) {
    vector<row_t > ret;
    row_t row(numcols, 0);
    
    for (int i = 1; i < numcols; i += 2) {
        row[i] = 1;
    }
    while (true) {
        ret.push_back(row);
        int pos = numcols - 1;
        while (row[pos] == 2 || (pos > 0 && row[pos] == 1 && row[pos - 1] == 2)) {
            --pos;
            if (pos < 0) break;
        }
        if (pos < 0) break;
        // pos is now the first place that we can advance
        row[pos]++;
        if (pos > 0 && row[pos] == row[pos - 1]) {
            row[pos]++;
        }
        assert(row[pos] < 3);
        for (int i = pos + 1; i < numcols; ++i) {
            if (row[i - 1] == 0) {
                row[i] = 1;
            } else {
                row[i] = 0;
            }
        }
    }
    return ret;
}

address_map_t get_address_map(const vector<row_t> &list) {
    address_map_t ret;
    for (int i = 0; i < list.size(); ++i) {
        ret[list[i]] = i;
    }
    return ret;
}

// return the index of the row that is gotten by setting the first two colors to 0 and 1 respectively.
index_t get_base(index_t row, const vector<row_t> &v, const address_map_t &index) {
    const row_t &rowv = v[row];
    row_t retv;
    vector<color> trans(3, 0); // color permutation
    trans[rowv[0]] = 0;
    trans[rowv[1]] = 1;
    trans[3 - (rowv[0] + rowv[1])] = 2;
    for (auto rit : rowv) {
        retv.push_back(trans[rit]);
    }
    return index.at(retv);
}

root_t weighted_root(const root_t &val, const vector<row_t> &v, const address_map_t &index) {
    root_t ret;
    const row_t &rowv = v[val.index];
    row_t retv;
    vector<color> trans(3, 0); // color permutation
    trans[rowv[0]] = 0;
    trans[rowv[1]] = 1;
    trans[3 - (rowv[0] + rowv[1])] = 2;
    for (color rit : rowv) {
        retv.push_back(trans[rit]);
    }
    ret.index = index.at(retv);
    ret.permutation[trans[0]] = val.permutation[0];
    ret.permutation[trans[1]] = val.permutation[1];
    ret.permutation[trans[2]] = val.permutation[2];
    return ret;
}
