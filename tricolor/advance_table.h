//
//  advance_table.h
//  tricolor
//
//  Created by David Einstein on 1/26/14.
//  Copyright (c) 2014 David Einstein. All rights reserved.
//

#ifndef __tricolor__advance_table__
#define __tricolor__advance_table__

#include <iostream>
#include <unordered_map>
#include "counted_row.h"
#include "check_match.h"
#include "row_gen.h"

struct dynamic_cell {
    index_t index;
    weight_t weights[3];
};

struct dynamic_cell_hash{
    size_t operator() (const dynamic_cell &row) const {
        size_t ret = row.index;
        for (int i = 0; i <3; ++i) {
            ret = 48 * ret + row.weights[i];
        }
        return ret;
    }
};

struct dynamic_cell_equal{
    size_t operator() (const dynamic_cell &a, const dynamic_cell &b) const {
        return a.index == b.index && a.weights[0] == b.weights[0] && a.weights[1] == b.weights[1]
        && a.weights[2] == b.weights[2];;
    }
};

// each possible arrangement is stored as a vector of 4 elements
// the index of the row, and the vector of weights
typedef std::unordered_map<dynamic_cell, mpz_class, dynamic_cell_hash, dynamic_cell_equal> dynamic_row;

dynamic_row next_row(const dynamic_row &last_row,
                     int num_rows,
                     const vector<index_list> &matches,
                     const vector<weight_list> &weights,
                     const int max_weight);

dynamic_row next_row_z(const dynamic_row &last_row,
                       int num_rows,
                       const vector<index_list> &matches,
                       const vector<weight_list> &weights,
                       const int max_weight,
                       const vector<root_t> &roots);


index_list get_table_at(mpz_class &pos, const vector<dynamic_row> &drows,
                         const vector<index_list> &matches,
                         const vector<weight_list> &weights);

index_list get_table_at_z(mpz_class &pos, const vector<dynamic_row> &drows,
                           const vector<index_list> &matches,
                           const vector<weight_list> &weights,
                           const vector<root_t> &roots,
                          const vector<newcolor_list> &newcolor_lists);

vector<std::pair<int, int> > list_color_pairs();
#endif /* defined(__tricolor__advance_table__) */
