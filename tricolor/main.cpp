//
//  main.cpp
//  tricolor
//
//  Created by David Einstein on 1/25/14.
//  Copyright (c) 2014 David Einstein. All rights reserved.
//

#include <iostream>
#include <algorithm>
#include <iterator>
#include <gmpxx.h>
#include "row_gen.h"
#include "check_match.h"
#include "counted_row.h"
#include "advance_table.h"

const int dimension = 12;


int main(int argc, const char * argv[])
{
    using std::cout;
    using std::transform;
    using std::copy;
    gmp_randclass rand(gmp_randinit_default);
    std::ostream_iterator<int> out_it(cout, " ");
    
    // make a list of all the possible rows (colored with three colors no two ajacent cells colored the same.
    vector<row_t> v = genrows(dimension);
    // make a map from admissible rows to the index in v;
    address_map_t index_map = get_address_map(v);

    // for each admissible row list the rows which can be adjacent to it
    vector<index_list> matches;
    for (auto vit = v.begin(); vit != v.end(); ++vit) {
        vector<row_t> match = get_matches(*vit, v);
        index_list transmatch;
        for (auto vvit = match.begin(); vvit != match.end(); ++vvit) {
            transmatch.push_back(index_map[*vvit]);
        }
        matches.push_back(transmatch);
    }
    
    vector<newcolor_list> newcolor_lists = make_newcolor_list(v, index_map);
    // for each admissible row count the number of 0's 1's and 2's
    vector<weight_list> weights(v.size());
    transform(v.begin(), v.end(), weights.begin(), get_count);
    
    vector<root_t> presolved_roots;
    root_t temp;
    temp.permutation[0] = 0;
    temp.permutation[1] = 1;
    temp.permutation[2] = 2;
    for (int i = 0; i < v.size(); ++i) {
        temp.index = i;
        presolved_roots.push_back(weighted_root(temp, v, index_map));
    }
#if 0
    // drows is a vector of maps from pairs of rows, and weights to counts
    // the i'th element of drows counts the number of (i+1) row rectangles whose
    // top row is specified, and whose total weight is specified.
    vector<dynamic_row> drows;
    for (int i = 0; i < dimension; ++i) {
        if (i == 0) {
            dynamic_row first_row;
            for (int j = 0; j < v.size(); ++j) {
                dynamic_cell elem(4);
                elem[0] = j;
                const weight_list &weight = weights[j];
                elem[1] = weight[0];
                elem[2] = weight[1];
                elem[3] = weight[2];
                first_row[elem] = 1;
            }
            drows.push_back(first_row);
        } else {
            dynamic_row curr_row = next_row(drows[i - 1], (int)v.size(), matches, weights,
                                            dimension * dimension / 3);
            drows.push_back(curr_row);
        }
    }
    auto pit = drows[dimension - 1].begin();
    int maxpos = pit->first[0];
    mpz_class maxval = pit->second;
    int minpos = pit->first[0];
    mpz_class minval = pit->second;
    mpz_class totval = pit->second;
    ++pit;
    while (pit != drows[dimension - 1].end()) {
        if (pit->second > maxval) {
            maxval = pit->second;
            maxpos = pit->first[0];
        }
        if (pit->second < minval) {
            minval = pit->second;
            minpos = pit->first[0];
        }
        totval += pit->second;
        ++pit;
    }
    copy(v[minpos].begin(), v[minpos].end(), out_it);
    cout << " - " << minval << std::endl;
    copy(v[maxpos].begin(), v[maxpos].end(), out_it);
    cout << " - " << maxval << std::endl;
    cout << totval << std::endl;
    
    for (int i = 0; i < 10; ++i) {
        mpz_class randpos = rand.get_z_range(totval);
        index_list randtable = get_table_at(randpos, drows, matches, weights);
        cout << "---------------------" << std::endl;
        for (auto rit = randtable.begin(); rit != randtable.end(); ++rit) {
            copy(v[*rit].begin(), v[*rit].end(), out_it);
            cout << std::endl;
        }
    }
#endif

    vector<dynamic_row> drows;
    for (int i = 0; i < dimension; ++i) {
        if (i == 0) {
            dynamic_row first_row;
            for (int j = 0; j < v.size(); ++j) {
                if (v[j][0] == 0 && v[j][1] == 1) {
                    dynamic_cell elem;
                    elem.index = j;
                    const weight_list &weight = weights[j];
                    elem.weights[0] = weight[0];
                    elem.weights[1] = weight[1];
                    elem.weights[2] = weight[2];
                    first_row[elem] = 6;
                }
            }
            drows.push_back(first_row);
        } else {
            dynamic_row curr_row = next_row_z(drows[i - 1], (int)v.size(), matches, weights,
                                            dimension * dimension / 3, presolved_roots);
            drows.push_back(curr_row);
        }
    }
    auto pit = drows[dimension - 1].begin();
    int maxpos = pit->first.index;
    mpz_class maxval = pit->second;
    int minpos = pit->first.index;
    mpz_class minval = pit->second;
    mpz_class totval = pit->second;
    ++pit;
    while (pit != drows[dimension - 1].end()) {
        if (pit->second > maxval) {
            maxval = pit->second;
            maxpos = pit->first.index;
        }
        if (pit->second < minval) {
            minval = pit->second;
            minpos = pit->first.index;
        }
        totval += pit->second;
        ++pit;
    }
    copy(v[minpos].begin(), v[minpos].end(), out_it);
    cout << " - " << minval << std::endl;
    copy(v[maxpos].begin(), v[maxpos].end(), out_it);
    cout << " - " << maxval << std::endl;
    cout << totval << std::endl;
    
    // OK lets look at half splitting
    const dynamic_row &row6 = drows[5];
    const dynamic_row &row7 = drows[6];
    auto temp6 = row6.begin();
    cout << (temp6->first.weights[0] + temp6->first.weights[1] +temp6->first.weights[2]) << std::endl;
    auto temp7 = row7.begin();
    cout << (temp7->first.weights[0] + temp7->first.weights[1] +temp7->first.weights[2]) << std::endl;
    totval = 0;
    for (auto dit = row7.begin(); dit != row7.end(); ++dit) {
        dynamic_cell val = dit->first;
        const weight_list &w = weights[val.index];
        val.weights[0] = 48 - val.weights[0] + w[0];
        val.weights[1] = 48 - val.weights[1] + w[1];
        val.weights[2] = 48 - val.weights[2] + w[2];
        auto d2it = row6.find(val);
        if (d2it != row6.end()) {
            // there are exactly 6 times too many values.  One half has a fixed color scheme once the other half has had its specified.
            totval += dit->second * d2it->second / 6;
        }
    }
    cout << totval << std::endl;

    for (int i = 0; i < 10; ++i) {
        mpz_class randpos = rand.get_z_range(totval);
        index_list randtable = get_table_at_z(randpos, drows, matches, weights, presolved_roots, newcolor_lists);
        if (!validate(randtable, v)) {
            cout << "failure" << std::endl;
        }
        cout << "---------------------" << std::endl;
        for (auto rit = randtable.begin(); rit != randtable.end(); ++rit) {
            copy(v[*rit].begin(), v[*rit].end(), out_it);
            cout << std::endl;
        }
    }

    return 0;
}

