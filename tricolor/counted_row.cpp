//
//  counted_row.cpp
//  tricolor
//
//  Created by David Einstein on 1/26/14.
//  Copyright (c) 2014 David Einstein. All rights reserved.
//

#include "counted_row.h"

weight_list get_count(const row_t &row) {
    weight_list ret(3, 0);
    for (color col : row) {
        ret[col]++;
    }
    return ret;
}

vector<newcolor_list> make_newcolor_list(const vector<row_t> &v, address_map_t &index) {
    vector<newcolor_list> ret;
    typedef vector<color> color_pair;
    vector<color_pair> pairs;
    for (coloring_t i = 0; i < 6; ++i) {
        color a = i / 2;
        color b = (a == 0 ? 1 : 0);
        if (i % 2 == 1) {
            ++b;
            if (a == b) ++b;
        }
        color_pair p(3);
        p[0] = a;
        p[1] = b;
        p[2] = 3 - (a + b);
        pairs.push_back(p);
    }
    
    for (auto base_row : v) {
        newcolor_list l;
        for (int i = 0; i < 6; ++i) {
            row_t row;
            for (color c : base_row ) {
                row.push_back(pairs[i][c]);
            }
            index_t pos = index[row];
            coloring_t coloring = 2 * row[0] + (row[1] == 0 || (row[0] == 0 && row[1] == 1) ? 0 :1);
            l.push_back(newcolor_t(pos, coloring));
        }
        ret.push_back(l);
    }
    return ret;
}