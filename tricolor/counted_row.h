//
//  counted_row.h
//  tricolor
//
//  Created by David Einstein on 1/26/14.
//  Copyright (c) 2014 David Einstein. All rights reserved.
//

#ifndef __tricolor__counted_row__
#define __tricolor__counted_row__

#include <iostream>
#include <vector>
#include <map>
#include "gmpxx.h"
#include "row_gen.h"

using std::map;
using std::vector;

typedef int8_t weight_t;
typedef vector<weight_t> weight_list;
weight_list get_count(const row_t &row);

typedef int8_t coloring_t;
typedef std::pair<index_t, coloring_t> newcolor_t;
typedef vector<newcolor_t> newcolor_list;

vector<newcolor_list> make_newcolor_list(const vector<row_t> &v, address_map_t &index);
#endif /* defined(__tricolor__counted_row__) */
