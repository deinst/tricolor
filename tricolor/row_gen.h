//
//  row_gen.h
//  tricolor
//
//  Created by David Einstein on 1/25/14.
//  Copyright (c) 2014 David Einstein. All rights reserved.
//

#ifndef __tricolor__row_gen__
#define __tricolor__row_gen__

#include <vector>
#include <unordered_map>
#include <iostream>

using std::vector;
using std::unordered_map;

typedef int8_t color;
typedef vector<color> row_t;
vector<row_t> genrows(int numcols);

typedef int16_t index_t;
typedef vector<index_t> index_list;

struct root_t {
    index_t index;
    color permutation[3];
};

struct row_hash{
    size_t operator() (const row_t &row) const {
        size_t ret = 0;
        for (auto it = row.begin(); it != row.end(); ++it) {
            ret = 3 * ret + *it;
        }
        return ret;
    }
};

typedef unordered_map<row_t, index_t, row_hash> address_map_t;
address_map_t get_address_map(const vector<row_t> &list);
index_t get_base(index_t row, const vector<row_t> &v, const address_map_t &index);
root_t weighted_root(const root_t &val, const vector<row_t> &v, const address_map_t &index);

#endif /* defined(__tricolor__row_gen__) */
